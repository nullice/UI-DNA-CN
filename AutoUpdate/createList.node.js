
const path = require("path")
const fs = require("fs")




if (process.argv[2] != undefined) {
    var fileName = process.argv[2]

} else {
    var fileName = "JSX_V0@6"
}

if (process.argv[3] != undefined) {
    var verindex = process.argv[3]
}
if (process.argv[4] != undefined) {
    var vername = process.argv[4]
}


if (process.argv[5] != undefined) {
    var zipname = process.argv[5]
}

if (process.argv[6] != undefined) {
    var dataname = process.argv[6].replace(/\-/g, "/")
}

if (process.argv[7] != undefined) {
    var patchname = process.argv[7]
}


console.log(__dirname, process.argv[2])

var SCAN_PATH = path.join(__dirname, fileName)
// var GIT_URL = "http://ui-dna.doc.design-enzyme.com"
var GIT_URL = "http://doc.design-enzyme.com"
var NOTSCAN = /^node_modules/


fileList = []

console.info("\x1b[94m", "扫描目录----------------------------", SCAN_PATH)
scanDir(SCAN_PATH)

function scanDir(inPath) {
    var dir = fs.readdirSync(inPath)
    for (x in dir) {
        if (NOTSCAN.test(dir[x])) {
            console.log("skip ", dir[x])
            continue;
        }


        var _thisPath = path.join(inPath, dir[x])
        var isDirectory = false
        try {
            var stat = fs.statSync(_thisPath)
            isDirectory = stat.isDirectory()
        } catch (e) {
            console.error(e)
        }

        if (isDirectory) {
            scanDir(_thisPath)
        } else {

            console.log("+", _thisPath)
            fileList.push("http://nullice.oschina.io/ui-dna-cn/AutoUpdate/" + fileName.replace("@", "%40") + "/" + path.relative(SCAN_PATH, _thisPath).replace(/\\/g, "/"))


        }

    }
}

console.log(fileList)
reg = /\/Proteins_libs\//
var jsxs = []
var proteins = []

for (x in fileList) {
    if (reg.test(fileList[x])) {
        proteins.push(fileList[x])
    } else {
        jsxs.push(fileList[x])
    }

}



var downloadJson = JSON.stringify({ jsxs, proteins }, null, 4)


console.log(downloadJson)


fs.writeFileSync(path.join(__dirname, fileName, "download.json"), downloadJson)


var sampleInfo = {}
sampleInfo[vername] = {
    "version": vername,
    "verIndex": verindex,
    "varData": dataname,
    "codename": "Euglena",
    "title": "修复-BUG",
    "lv": "Beta",
    "changeList": "更新说明",
    "url": "http://design-enzyme.com/UI-DNA/#/download",
    "downloads": [
        {
            "name": "默认线路",
            "direct": true,
            "url": "http://nullice.oschina.io/ui-dna-cn/" + zipname
        },
        {
            "name": "备用",
            "direct": true,
            "info": "会消耗本站大量流量，流量很贵的...",
            "url": "http://obp804p1n.bkt.clouddn.com/" + zipname
        },
        {
            "name": "备用2",
            "direct": true,
            "url": "http://nullice.coding.me/UI-DNA-CN/" + zipname
        }
    ],
    "downloads_exe": {
        "windows": "http://nullice.oschina.io/ui-dna-cn/UIDNA-" + vername + "-windows-installer.exe",
        "osx": "http://nullice.oschina.io/ui-dna-cn/UIDNA-" + vername + "-osx-installer.app.zip"
    },
    "autoUpdate": {
        "minBaseVer": 5,
        "url": "http://nullice.oschina.io/ui-dna-cn/AutoUpdate/" + patchname.replace("@", "%40"),
        "fileName": patchname,
        "jsxs": "http://nullice.oschina.io/ui-dna-cn/AutoUpdate/JSX_V0%40" + verindex + "/download.json"
    }
}

console.info("\x1b[97m" + JSON.stringify(sampleInfo, null, 4))
console.log("\x1b[94m")





